#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 22
#define TEMPERATURE_PRECISION 11

float tempin = 0;
float tempext = 0;

int sensorCount = 0;

bool debug = false;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress sensor[147];

void setup(void)
{
  // start serial port
  Serial.begin(9600);

  sensors.begin();
  sensorCount = sensors.getDeviceCount();
  if (debug){
    Serial.print("Se encontraron ");
    Serial.print(sensorCount, DEC);
    Serial.println(" sensores.");
  }
  for(int i = 0; i<sensorCount; i++){
    if (!sensors.getAddress(sensor[i], i)) Serial.println("Sensor no conectado"); //Esto creo que es inútil
    sensors.setResolution(sensor[i], TEMPERATURE_PRECISION);
  }

  if(debug){
    Serial.print("La precisión es:  ");
    Serial.println(TEMPERATURE_PRECISION);
  }
}

void loop(void)
{
  sensors.requestTemperatures();

    for(int i = 0; i<sensorCount; i++){
      Serial.print(sensors.getTempC(sensor[i]));
      if(i == (sensorCount - 1)){
        Serial.println("");
      } else {
        Serial.print(",");
      }
    }

  if(debug){
    delay(2000);
  }
}
