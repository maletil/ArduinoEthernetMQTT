#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 22
#define TEMPERATURE_PRECISION 11

float tempin = 0;
float tempext = 0;

int sensorCount = 0;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress sensor0, sensor1, sensor2;

void setup(void)
{
  // start serial port
  Serial.begin(9600);

  sensors.begin();
  sensorCount = sensors.getDeviceCount();
  Serial.print("Se encontraron ");
  Serial.print(sensorCount, DEC);
  Serial.println(" sensores.");

  if (!sensors.getAddress(sensor0, 0)) Serial.println("Sensor 0 no conectado");
  if (!sensors.getAddress(sensor1, 1)) Serial.println("Sensor 1 no conectado");
  if (!sensors.getAddress(sensor2, 2)) Serial.println("Sensor 2 no conectado");
  sensors.setResolution(sensor0, TEMPERATURE_PRECISION);
  sensors.setResolution(sensor1, TEMPERATURE_PRECISION);
  sensors.setResolution(sensor2, TEMPERATURE_PRECISION);

  Serial.print("La precisión es:  ");
  Serial.println(TEMPERATURE_PRECISION);
}
void loop(void)
{
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  //Serial.print("Requesting temperatures...");
  sensors.requestTemperatures();

  Serial.print(sensors.getTempC(sensor0));
  Serial.print(",");
  Serial.print(sensors.getTempC(sensor1));
  if (sensors.getAddress(sensor2, 2)){
    Serial.print(",");
    Serial.println(sensors.getTempC(sensor2));
  } else{
    Serial.println("");
  }
}
