#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>
#include <OneWire.h>
#include <DallasTemperature.h>

IPAddress MQTT_SERVER(192, 168, 1, 42);
byte ip[] = { 192, 168, 1, 120 };
byte mac[] = { 0x91, 0xA2, 0xDA, 0x0D, 0x31, 0xB8 };

EthernetClient ethClient;
PubSubClient client(ethClient);

void sendMQTT(char* topic, char* message, bool newline=true);
void logMsg(String string ,bool dashed=false, bool newline=true);

const char* username = "";
const char* password = "";
const char* clientID = "";

bool retained = true;

char* subscribeList[] = {"a", "b"};
long resetTopic = "r";
long infoTopic = "i";
String sensorTopic = "sensor";

char* pinout[][2] = {
  {"a","12"},
  {"b","25"},
  {"c","23"},
  {"d","13"},
  {"e","33"}
};
int ledPin = 13;
int resetPin = 10;

//Temperatura
 
#define ONE_WIRE_BUS 22
#define TEMPERATURE_PRECISION 11
int sensorCount = 0;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress sensor[147];

unsigned long time;
void setup()
{
  // initialize the digital pin as an output.
  digitalWrite(resetPin, HIGH);
  pinMode(ledPin, OUTPUT);
  pinMode(resetPin, OUTPUT);
  for (int i = 0; i < sizeof(pinout)/4; i++){
    int pinSet = atoi(pinout[i][1]);
    pinMode(pinSet, OUTPUT);
  }
  pinMode(LED_BUILTIN, OUTPUT);

  // init serial link for debugging
  Serial.begin(9600);  
  logMsg("Iniciando.", true);
  Ethernet.begin(mac, ip);

  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);

  sensors.begin();
  sensorCount = sensors.getDeviceCount();
  Serial.print("Se encontraron ");
  Serial.print(sensorCount, DEC);
  Serial.println(" sensores.");
  for(int i = 0; i<sensorCount; i++){
    if (!sensors.getAddress(sensor[i], i)) Serial.println("Sensor no conectado");
    sensors.setResolution(sensor[i], TEMPERATURE_PRECISION);
  }
}

char message_buff[100];

void loop()
{
  if (!client.connected())
  {
    Serial.println("Conectando con el broker MQTT...");
    digitalWrite(LED_BUILTIN, HIGH);
      // clientID, username, password
      if (client.connect(clientID, username, password)) {
        Serial.println("");
        logMsg("Se ha conectado correctamente.", false, false);
        sendMQTT(infoTopic, "En línea", false);
        sendMQTT(resetTopic, "0");

        Serial.println("Topics a los que se suscribe");

        for (int i = 0; i < sizeof(subscribeList)/2; i++){
          client.subscribe(subscribeList[i]);
          String subsStr(subscribeList[i]);
          Serial.print("  ");
          Serial.println(subsStr);
        }
        for (int i = 0; i < sizeof(pinout)/4; i++){
          client.subscribe(pinout[i][0]);
          String subsStr(pinout[i][0]);
          Serial.print("  ");
          Serial.println(subsStr);
        }
        
        Serial.println("");
        digitalWrite(LED_BUILTIN, LOW);
      } else {
        logMsg("Error en la conexión");
      }
  }
      if (client.connected() && sensorCount != 0){
        if (millis() > (time + 10000)) {
          time = millis();
            digitalWrite(ledPin, HIGH);
          sensors.requestTemperatures();
          for(int i = 0; i<sensorCount; i++){
            float tempFloat = (sensors.getTempC(sensor[i]));
            char temp[8];
            dtostrf(tempFloat, 6, 2, temp);

            String topic = sensorTopic;
            topic += i + 1; //Los topics empiezan desde i + 1 : 1
            char topicChar[50];
            topic.toCharArray(topicChar, 50);
            if(i == (sensorCount - 1)){
              sendMQTT(topicChar, temp);
            } else {
              sendMQTT(topicChar, temp, false);
            }
          }
            digitalWrite(ledPin, LOW);
        }
      }

  client.loop();
}

void callback(char* topic, byte* payload, unsigned int length) {
  digitalWrite(ledPin, HIGH);
  Serial.print("<- ");
  int i = 0;
  for(i=0; i<length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';
  String msgString = String(message_buff);
  String topicStr(topic);
  Serial.println(topicStr + " " + msgString);

  if (topicStr == resetTopic && msgString == "1"){
      Serial.println("");
      sendMQTT(resetTopic, "0");
      Serial.println("Se va a reiniciar");
      sendMQTT(infoTopic, "Reinicio");
      digitalWrite(resetPin, LOW);
  } else if(topicStr == infoTopic || topicStr == resetTopic) {
      Serial.println("");
  } else{
    writeInput(msgString, topic);
  }
  delay(100);
  digitalWrite(ledPin, LOW);
}

void sendMQTT(char* topic, char* message, bool newline=true){
  String topicStr(topic);
  Serial.print("-> ");
  Serial.print(topicStr);
  Serial.print(" ");
  Serial.println(message);
  if(newline){
    Serial.println("");
  }
  client.publish(topic, message, retained);
}

void logMsg(String string ,bool dashed=false, bool newline=true){
  if(dashed){
    Serial.println("------------------");
  }
  Serial.println(string);
  if(newline){
    Serial.println("");
  }
}

void writeInput(String value, char* topic){
  char* pinChar = findInArray(topic, pinout);
  if (pinChar == "notfound"){
    logMsg("El pin no está registrado!");
  } else {
    int pin = atoi(pinChar);
    if(value == "1"){
      digitalWrite(pin, HIGH);
    } else if (value == "0") {
      digitalWrite(pin, LOW);
    }
    logMsg("Se efectua " + value + " en " + pinChar + " por " + topic);
  }
}
bool found;
char* findInArray(char* searchStr, char* inArray[][2]){
  found = false;
  for(int i = 0; i < sizeof(inArray)*4; i++){
    String row = inArray[i][0];
    if (row == searchStr){
      found = true;
      return inArray[i][1];
    }
  }
  found = false;
  return "notfound";
}
