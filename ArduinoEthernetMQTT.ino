#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>

IPAddress MQTT_SERVER(192, 168, 1, 104); 
byte ip[] = { 192, 168, 1, 10 }; 
byte mac[] = { 0x91, 0xA4, 0xDA, 0x0D, 0x31, 0xB8 }; 

EthernetClient ethClient;
PubSubClient client(ethClient);

void sendMQTT(char* topic, char* message, bool newline=true);
void logMsg(String string ,bool dashed=false, bool newline=true);

const char* username = "";
const char* password = "";
const char* clientID = "";

char* subscribeList[] = {"a", "b", "c"};
long resetTopic = "r";
long infoTopic = "i";

char* pinout[][2] = {
  {"a","7"},
  {"b","14"},
  {"c","25"}
};
int ledPin = 9;
int resetPin = 10;

unsigned long time;
void setup()
{
  // initialize the digital pin as an output.
  digitalWrite(resetPin, HIGH);
  pinMode(ledPin, OUTPUT);
  pinMode(resetPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // init serial link for debugging
  Serial.begin(9600);

  logMsg("Iniciando.", true);
  Ethernet.begin(mac, ip);

  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);
}

char message_buff[100];

void loop()
{
  if (!client.connected())
  {
    Serial.println("Conectando con el broker MQTT...");
    digitalWrite(LED_BUILTIN, HIGH);
      // clientID, username, password
      if (client.connect(clientID, username, password)) {
        Serial.println("");
        logMsg("Se ha conectado correctamente.", false, false);
        sendMQTT(infoTopic, "En línea", false);
        sendMQTT(resetTopic, "0");

        Serial.println("Topics a los que se suscribe");

        for (int i = 0; i < sizeof(subscribeList)/2; i++){
          client.subscribe(subscribeList[i]);
          String subsStr(subscribeList[i]);
          Serial.print("  ");
          Serial.println(subsStr);
        }
        Serial.println("");
        digitalWrite(LED_BUILTIN, LOW);
      } else {
        logMsg("Error en la conexión");
      }

  }
      if (client.connected()){
        int number = random(256);
        // publish random numbers every 5 seconds
        if (millis() > (time + 5000)) {
          time = millis();
          String pubString = "Número " + String(number);
          pubString.toCharArray(message_buff, pubString.length()+1);
          sendMQTT("caldera/number", message_buff);
        }
      }

  client.loop();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("<- ");
  int i = 0;
  for(i=0; i<length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';
  String msgString = String(message_buff);
  String topicStr(topic);
  Serial.println(topicStr + " " + msgString);
  Serial.println("");


  if (topicStr == resetTopic && msgString == "1"){
      sendMQTT(resetTopic, "0");
      Serial.println("Se va a reiniciar");
      sendMQTT(infoTopic, "Reinicio");
      digitalWrite(resetPin, LOW);
  } else {
      writeInput(msgString, topic);
  }
}

void sendMQTT(char* topic, char* message, bool newline=true){
  String topicStr(topic);
  Serial.print("-> ");
  Serial.print(topicStr);
  Serial.print(" ");
  Serial.println(message);
  if(newline){
    Serial.println("");
  }
  client.publish(topic, message);
}

void logMsg(String string ,bool dashed=false, bool newline=true){
  if(dashed){
    Serial.println("------------------");
  }
  Serial.println(string);
  if(newline){
    Serial.println("");
  }
}

void writeInput(String value, char* topic){
  char* pinChar = findInArray(topic, pinout);
  if (pinChar == "notfound"){
    logMsg("El pin no está registrado!");
  } else {
    int pin = atoi(pinChar);
    if(value == "1"){
      digitalWrite(pin, HIGH);
      Serial.println("UNO");
      Serial.println(pin);
    } else if (value == "0") {
      digitalWrite(pin, LOW);
    }
    Serial.println("Se efectua " + value + " en " + pinChar + " por " + topic);
  }
}

char* findInArray(char* searchStr, char* inArray[][2]){
  bool found = false;
  for(int i = 0; i < sizeof(inArray)*2; i++){
    String row = inArray[i][0];
    if (row == searchStr){
      found = true;
      return inArray[i][1];
    }
  }
  found = false;
  return "notfound";
}
