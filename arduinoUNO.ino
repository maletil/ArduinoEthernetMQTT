#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>


IPAddress MQTT_SERVER(192, 168, 1, 104);
byte ip[] = { 192, 168, 1, 10 };

// MAC Address of Arduino Ethernet Sheild (on sticker on shield)
byte mac[] = { 0x91, 0xA7, 0xDA, 0x0D, 0x31, 0xB8 };

EthernetClient ethClient;
PubSubClient client(ethClient);

const char* username = "pi";
const char* password = "1234";
const char* clientID = "uno";


long resetTopic = "r";
long infoTopic = "i";

// Pin 2 is the LED output pin
int ledPin = 9;
// Pin 8 is connected to RESET
int resetPin = 10;

// defines and variable for sensor/control mode
#define MODE_OFF    0  // not sensing light, LED off
#define MODE_ON     1  // not sensing light, LED on
#define MODE_SENSE  2  // sensing light, LED controlled by software
int senseMode = 0;

unsigned long time;

char message_buff[100];

void setup()
{
  // initialize the digital pin as an output.
  digitalWrite(resetPin, HIGH);
  pinMode(ledPin, OUTPUT);
  pinMode(resetPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // init serial link for debugging
  Serial.begin(9600);

  Serial.println("Iniciando");
  Ethernet.begin(mac, ip);

  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);
}

void loop()
{
  if (!client.connected())
  {
    Serial.println("Se va a conectar al broker MQTT");
    digitalWrite(LED_BUILTIN, HIGH);
      // clientID, username, password
      if (client.connect(clientID, username, password)) {
        client.publish(infoTopic, "En línea");
        client.publish(resetTopic, "0");
        client.subscribe("almacen/puerta");
        client.subscribe(resetTopic);
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("Se ha conectado correctamente.");
      } else {
        Serial.println("Error en la conexión");
      }

  }

  switch (senseMode) {
    case MODE_OFF:
      // light should be off
      digitalWrite(ledPin, LOW);
      break;
    case MODE_ON:
      // light should be on
      digitalWrite(ledPin, HIGH);
      break;

  }
      int number = random(256);

      // publish light reading every 5 seconds
      if (millis() > (time + 5000)) {
        time = millis();
        String pubString = "Número " + String(number);
        pubString.toCharArray(message_buff, pubString.length()+1);
        Serial.println(pubString);
        client.publish("almacen/number", message_buff);
      }


  // MQTT client loop processing
  client.loop();
}

// handles message arrived on subscribed topic(s)
void callback(char* topic, byte* payload, unsigned int length) {

  Serial.println("");
  Serial.println("Callback!");
  int i = 0;

  //Serial.println("Message arrived:  topic: " + String(topic));
  //Serial.println("Length: " + String(length,DEC));

  // create character buffer with ending null terminator (string)
  for(i=0; i<length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';

  String msgString = String(message_buff);

  String topicStr(topic);

  Serial.println(topicStr + " " + msgString);


  if (topicStr == resetTopic){
    if (msgString == "1"){
      client.publish(resetTopic, "0");
      Serial.println("Se va a reiniciar");
      client.publish(infoTopic, "Reinicio");
      digitalWrite(resetPin, LOW);
    }
  } else {
    if(msgString == "1") {
      senseMode = MODE_ON;
      Serial.println("MODE_ON");
    } else if (msgString == "0") {
      senseMode = MODE_OFF;
      Serial.println("MODE_OFF");
    }
  }
}
